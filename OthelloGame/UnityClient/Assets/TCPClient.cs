﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Threading;
using System.Collections;
using System.IO;
using System.Net.Sockets;
using UnityEngine;
using Othello_Binary_Buffer;
//using System.Runtime.Serialization.Formatters.Binary;
//using Google.ProtocolBuffers.Examples.Othello;


namespace TCPClient
{
    public class Client
    {
        public Client()
        {
            //joinReqBuilder = JoinGameRequest.CreateBuilder();
            //joinResBuilder = JoinGameResponse.CreateBuilder();
            //inGameReqBuilder = InGameRequest.CreateBuilder();
            //inGameRespBuilder = InGameResponse.CreateBuilder();

            joinedRoom = false;
            resetVariables();

        }


        //Packet vairables
        byte[] data;
        bool joinedRoom = false;
        bool completePacket = true;
        int header_left = 0;
        int payload_left = 0;
        int payload_length = 0;

        byte[] cache_data = new byte[1024];
        int cache_payload_left = 0;
        int cache_length = 0;
        int cache_header_left = 0;


        //Making builders of protofiles
        //JoinGameRequest.Builder joinReqBuilder;
        //JoinGameResponse.Builder joinResBuilder;
        //InGameRequest.Builder inGameReqBuilder;
        //InGameResponse.Builder inGameRespBuilder;



        bool socketReady = false;

        TcpClient mySocket;
        NetworkStream theStream;
        StreamWriter theWriter;
        StreamReader theReader;
        String Host = "127.0.0.1";
        Int32 Port = 843;
        Grid board;

        //Send to Redis
        public static string HttpGet(string URI, string Parameters)
        {
            System.Net.WebRequest req = System.Net.WebRequest.Create(URI);
            req.Proxy = new System.Net.WebProxy();
            //Add these, as we're doing a POST
            req.ContentType = "application/x-www-form-urlencoded";
            req.Method = "GET";
            //We need to count how many bytes we're sending. 
            //Post'ed Faked Forms should be name=value&
            byte[] bytes = System.Text.Encoding.ASCII.GetBytes(Parameters);
            req.ContentLength = bytes.Length;
            System.IO.Stream os = req.GetRequestStream();
            os.Write(bytes, 0, bytes.Length); //Push it out there
            os.Close();
            System.Net.WebResponse resp = req.GetResponse();
            if (resp == null) return null;
            System.IO.StreamReader sr =
                  new System.IO.StreamReader(resp.GetResponseStream());
            return sr.ReadToEnd().Trim();
        }


        public void resetVariables()
        {
            completePacket = true;
            header_left = 0;
            payload_left = 0;
            payload_length = 0;

            cache_data = new byte[1024];
            cache_payload_left = 0;
            cache_length = 0;
            cache_header_left = 0;



        }

        public void setGrid(Grid value)
        {
            board = value;
        }

        public void readFromServer()
        {
            while (true)
            {
                byte[] bytesRcv = readSocketInBytes();
                Deserialize(bytesRcv);
            }



        }


        //Will prepare protobuf object and send 
        public void coinToPlace(string cellName)
        {
            //BinaryFormatter formatter = new BinaryFormatter();
            Othello_Buffer buffer = new Othello_Buffer();

            buffer.requestType = "INREQ";
            buffer.cellno = cellName;
            buffer.flippedCells = new List<string>();
            //inGameReqBuilder = InGameRequest.CreateBuilder();
            //inGameReqBuilder.SetRequestType("INREQ");
            //inGameReqBuilder.SetCellno(cellName);

            //InGameRequest ireq = inGameReqBuilder.BuildPartial();
            //inGameReqBuilder = null;
            byte[] len;
            byte[] payload;
            long length;
            //using (MemoryStream stream = new MemoryStream())
            //{
            //    //Save the person to a stream
            //   // ireq.WriteTo(stream);
            //    formatter.Serialize(stream, buffer);

            //    payload = stream.ToArray();
            //}
            string serialized = JSONHandler.SerializeTCPMessage(buffer);
            payload = System.Text.Encoding.ASCII.GetBytes(serialized);
            length = payload.Length;
            len = BitConverter.GetBytes(length);


            byte[] message = new byte[len.Count() + payload.Count() + 1];
            byte delimeter = 28;

            len.CopyTo(message, 0);
            payload.CopyTo(message, len.Length);
            message[message.Length - 1] = delimeter;

            writeSocket(message);

        }

        static byte[] trim(byte[] bytes)
        {
            int a = 0;
            byte delimeter = 28;
            int index = Array.LastIndexOf(bytes, delimeter);
          
            Array.Resize(ref bytes, index);
            return bytes;
        }

        bool processPacket(byte[] dataRcv)
        {
            //Get Complete Buffer data from clientManager Map 

            byte[] temp = dataRcv;
            byte[] bufferToProcess = new byte[payload_length];
            Array.Copy(temp, 8, bufferToProcess, 0, payload_length);

            Othello_Buffer buffer = new Othello_Buffer();
        
            string deserialized = System.Text.Encoding.ASCII.GetString(bufferToProcess);
            buffer = JSONHandler.DeSerializeJSONString(deserialized);



            if (buffer.requestType == "JOINRESP")
            {
                joinedRoom = true;
                board.playerIndex = buffer.playerIndex;

                if (board.playerIndex == 0)
                {
                    board.playerCount = buffer.playerOneCount;
                    board.enemyCount = buffer.playerTwoCount;
                }
                else
                {
                    board.enemyCount = buffer.playerOneCount;
                    board.playerCount = buffer.playerTwoCount;
                }
            
            }
            else if (buffer.requestType == "INGAMERESP")
            {
                //This is in Game request
                if (buffer.status == "INVALID")
                {
                    //Move is not valid

                }
                else if (buffer.status == "CHANGETURN")
                { 
                    //No moves left .change turn 
                    board.index = buffer.playerIndex;
                
                
                
                }
                else if (buffer.status == "VALID")
                {
                    //Move is valid . Now Instatiate cell and flip required coins
                    board.dataFromServer = true;
                    board.instantiateObj = buffer.instantiateCell;
                    board.index = buffer.playerIndex;

                    if (board.playerIndex == 0)
                    {
                        board.playerCount = buffer.playerOneCount;
                        board.enemyCount = buffer.playerTwoCount;
                    }
                    else
                    {
                        board.enemyCount = buffer.playerOneCount;
                        board.playerCount = buffer.playerTwoCount;
                    }


                        //  board.coinsToFlip = buffer.flippedCells;
                        for (int i = 0; i < buffer.flippedCells.Count(); i++)
                        {
                            board.coinsToFlip.Add(buffer.flippedCells[i]);


                        }


                }
                else if (buffer.status == "GAMEENDS")
                {
                    board.dataFromServer = true;
                    board.instantiateObj = buffer.instantiateCell;
                    board.index = buffer.playerIndex;

                    if (board.playerIndex == 0)
                    {
                        board.playerCount = buffer.playerOneCount;
                        board.enemyCount = buffer.playerTwoCount;
                    }
                    else
                    {
                        board.enemyCount = buffer.playerOneCount;
                        board.playerCount = buffer.playerTwoCount;
                    }


                    //  board.coinsToFlip = buffer.flippedCells;
                    for (int i = 0; i < buffer.flippedCells.Count(); i++)
                    {
                        board.coinsToFlip.Add(buffer.flippedCells[i]);


                    }


                    //Game ends . Winners is........
                    board.gameEnds = true;
                    if (Convert.ToInt32(buffer.winner) == board.playerIndex)
                    {
                        board.message = "YOU HAVE WON !!! Congrats!!! :D ";

                    }
                    else
                    {
                        board.message = "You have been defeated !!!! :( ";
                    
                    }
                }
                else if(buffer.status == "GAMESTART")
                {
                    board.gameStarted = true;
                }
            }


          
            return true;
        }


        bool Deserialize(byte[] bytes)
        {
            data = trim(bytes);
            bool cachePresent = false;
            do
            {
                // clientInfo.packetData = trim(clientInfo.packetData);


                if (data.Count() + header_left < 8)
                {
                    // Header hasnt been received 
                    header_left = 8 - data.Count();
                    completePacket = false;

                    //Read rest of header 

                    theStream.Read(data, data.Count(), header_left);


                }
                else
                {
                    byte[] length = data;
                    Array.Resize(ref length, 8);

                    int len = BitConverter.ToInt32(length, 0);
                    payload_length = len;
                    if (data.Count() + payload_left < len + 8)
                    {
                        //Partial payload received
                        payload_left = (len + 8) - (data.Count());
                        completePacket = false;
                        int currentSize = data.Count();

                        if (len > data.Count())
                        {
                            Array.Resize(ref data, len);
                        }
                        //Read rest of payload 
                        theStream.Read(data, currentSize, payload_left);

                    }
                    else if (data.Count() > len + 8)
                    {
                        //partial second packet received
                        Array.Copy(data, payload_length, cache_data, 0, data.Count() - payload_length);
                        cache_data = trim(cache_data);

                        cachePresent = true;
                        
                        completePacket = true;
                       
                    }
                    else
                    {
                        //clientInfo.packetData.Count() + clientInfo.payload_left == len + 8

                        //Proper packet 

                        completePacket = true;
                    }

                }
            } while (!completePacket);





            processPacket(data);

            //Resets varaibles
            resetVariables();

            //Process any extra packets... Since this will be recursive pattern ... it will handle multiple packets in one buffer
            if (cachePresent)
            {
                Deserialize(cache_data);
            }

           
            return true;
        }


        public void setupSocket()
        {
            try
            {

                mySocket = new TcpClient(Host, Port);
                theStream = mySocket.GetStream();
    
                socketReady = true;

                Othello_Buffer buffer = new Othello_Buffer();

                buffer.requestType = "JOINREQ";
                buffer.name = board.playerName;
                buffer.flippedCells = new List<string>();

             
                byte[] len;
                byte[] payload;
                long length;
            
                string serialized = JSONHandler.SerializeTCPMessage(buffer);
                payload = System.Text.Encoding.ASCII.GetBytes(serialized);
                length = payload.Length;
                len = BitConverter.GetBytes(length);

                Debug.Log(payload.Count());
                byte[] message = new byte[len.Count() + payload.Count() + 1];
                byte delimeter = 28;

                len.CopyTo(message, 0);
                payload.CopyTo(message, len.Length);
                message[message.Length - 1] = delimeter;

                Debug.Log(message.Count());
                writeSocket(message);
                //Fetch join ack 
                byte[] bytesRcv = readSocketInBytes();
                Deserialize(bytesRcv);
                //Start thread to read Response
                Thread thread = new Thread(new ThreadStart(readFromServer));
                thread.Start();

            }
            catch (Exception e)
            {
                Debug.Log("Socket error:" + e);
            }
        }

        public void writeSocket(byte[] sendBytes)
        {
            if (!socketReady)
                return;

            theStream.Write(sendBytes, 0, sendBytes.Length);
            theStream.Flush();
            //theWriter.Write(tmpString);
            //theWriter.Flush();
            Debug.Log("packet sent");
        }

        public byte[] readSocketInBytes()
        {
            byte[] bytesFrom = new byte[1024];
            theStream.Read(bytesFrom, 0, 1024);


            return bytesFrom;
        }

    
        public String readSocket()
        {
            if (!socketReady)
                return "";
            if (theStream.DataAvailable)
                return theReader.ReadLine();
            return "";
        }

        public void closeSocket()
        {
            if (!socketReady)
                return;
            theWriter.Close();
            theReader.Close();
            mySocket.Close();
            socketReady = false;
        }

        public void maintainConnection()
        {
            if (!theStream.CanRead)
            {
                setupSocket();
            }
        }
    }
}
