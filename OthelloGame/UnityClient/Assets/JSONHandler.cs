﻿using System;
using System.Collections.Generic;
using LitJson;
using Othello_Binary_Buffer;

class JSONHandler
{
	public static string SerializeTCPMessage(Othello_Buffer buffer)
	{
        return JsonMapper.ToJson(buffer);
	}


    public static Othello_Buffer DeSerializeJSONString(string jsonString)
	{
        Othello_Buffer deserializedTCPMessage = new Othello_Buffer();//new TCPMessage(null, -1, "", TCPMessage.Player.Computer, TCPMessage.MessageType.RollDie);
		deserializedTCPMessage = JsonMapper.ToObject<Othello_Buffer>(jsonString);
		return deserializedTCPMessage;
	}


}

