﻿using UnityEngine;
using System.Collections;
using TCPClient;

public class GUI_HighScoreButton : MonoBehaviour {

      public WWW GET(string url)
     {
 
     WWW www = new WWW (url);
     StartCoroutine (WaitForRequest (www));
     return www; 
     }
 
     //public WWW POST(string url, Dictionary<string,string> post)
     //{
     //WWWForm form = new WWWForm();
     //foreach(KeyValuePair<String,String> post_arg in post)
     //{
     //   form.AddField(post_arg.Key, post_arg.Value);
     //}
     //    WWW www = new WWW(url, form);
 
     //    StartCoroutine(WaitForRequest(www));
     //return www; 
     //}
 
     private IEnumerator WaitForRequest(WWW www)
     {
         yield return www;
 
         // check for errors
         if (www.error == null)
         {
             Debug.Log("WWW Ok!: " + www.text);
         } else {
             Debug.Log("WWW Error: "+ www.error);
         }    
     }
 


    void OnGUI()
    {




        if (GUI.Button(new Rect(10, 10, 50, 50), "HighScore"))
        {
            if (Security.PrefetchSocketPolicy("127.0.0.1", 5656))
            {
                Debug.Log("Policy found");
                WWW resp = GET("http://127.0.0.1:5657/?type=highscore&name=nish&");
            
            }
          //  string a = Client.HttpGet("http://127.0.0.1:5657/", "type=highscore&name=nish&");

         
            Debug.Log("Clicked the button with an image");
        }

        if (GUI.Button(new Rect(10, 70, 50, 30), "My Score"))
        {
            Debug.Log("Clicked the button with text");
        }

    }
}
