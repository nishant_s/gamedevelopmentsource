﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TCPClient;
//using UnityEditor;
using Facebook;


public class Grid : MonoBehaviour {
	public GameObject prefab;
	public GameObject coin;
	public int coinCount = 0;
	//public char map[,]= new char[8,8];
	public string[,] grid = new string[8,8];
	GameObject block1  ; 
	float colldWidth    = 8.0f;
	float colldHeight   = 8.0f;
	float  spawnSpeed  = 1;
	

    //Info from server 
   public bool isBlack = true;
   public int playerIndex = 0;
   public int index = 3 ;
   public bool dataFromServer = false;
   public bool gameStarted = false;
   public bool gameEnds = false;
   public bool pause = false;
   public string message = "Nothing";
   public string instantiateObj = "TBA";
   public IList<string> coinsToFlip = new List<string>();
   public int playerCount = 2;
   public int enemyCount = 2;
   public Texture btnTextureB;
   public Texture btnTextureW;



   public string playerName = "Player";




	Client c = new Client();

   

	void Start () {
		
		Createworld();
		Debug.Log ("before connect");
        c.setGrid(this);
        c.setupSocket ();
       // enabled = false;
		Debug.Log ("after connect");
	}
	
	
	void Update ()
    {

        OnGUI();

        if (!pause)
        {
            if (gameEnds)
            {
                //if (EditorUtility.DisplayDialog(message, "OK", "Cancel"))
                //{
                //    //Share button
                //    FB.Feed(linkCaption: "I just won a round of Othello !!!!"); 
                
                //}


            }
            else if(gameStarted)
            {
                if (Input.GetButtonDown("Fire1"))
                {
                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hit;// = Physics.Raycast(ray, 30);
                    if (Physics.Raycast(ray, out hit))
                    {
                        if ((index == 3 || index == 1) && (0 == playerIndex)) //((index == 3 || index == 1) && (playerIndex == 0))
                        {
                            GameObject obj = hit.collider.gameObject;
                            c.coinToPlace(obj.name);
                        }
                        else if (index == 0 && !(playerIndex == 0))
                        {
                            GameObject obj = hit.collider.gameObject;
                            c.coinToPlace(obj.name);
                        }

                    }
                }


                if (dataFromServer)
                {
                    dataFromServer = !dataFromServer;
                    inGameResponse();
                    coinsToFlip.Clear();


                }
            }
        }
	}



    public void OnGUI()
    {
        if (gameStarted && !gameEnds)
        {
             message = "Score -| You : " + playerCount + " | Opponent : " + enemyCount + " | ";
            GUI.Label(new Rect(Screen.width / 2 + 50, Screen.height / 2 + 120, 200, 100), message);

            if (((index == 3 || index == 1) && (0 == playerIndex)) || (index != 3 && (playerIndex != index)))
            {
                GUI.backgroundColor = Color.yellow;
                GUI.Label(new Rect(Screen.width / 2 + 50, Screen.height / 2 + 150, 100, 100), "Your Turn");

            }
            else
            {
                GUI.backgroundColor = Color.yellow;
                GUI.Label(new Rect(Screen.width / 2 + 50, Screen.height / 2 + 150, 100, 100), "Opponent's Turn");

            }
        }
        else if (gameEnds)
        {

            GUI.Label(new Rect(Screen.width / 2 + 50, Screen.height / 2 + 120, 200, 100), message);

        }
        else 
        {
            message = "Waiting for opponent...........";
            GUI.Label(new Rect(Screen.width / 2 + 50, Screen.height / 2 + 120, 200, 100), message);
        }
       
    }

	
	

    public void inGameResponse()
    {
        Debug.Log(instantiateObj);
        GameObject cell = GameObject.Find(instantiateObj);
        //Debug.Log(cell.name);
        Block b = cell.GetComponent<Block>();
        b.isOccupied = true;
        b.playerIndex = index;
    
        GameObject coin2 = coin;

        coin2 = Instantiate(coin2, new Vector3(cell.transform.position.x, cell.transform.position.y + 1, cell.transform.position.z), coin.transform.rotation) as GameObject;

        if (index == 0)
        {
            coin2.transform.renderer.material.color = Color.blue;

        }
        else
        {
            coin2.transform.renderer.material.color = Color.red;
        }
        coin2.name = "coin" + coinCount;
        b.coinName = coin2.name;

        //isBlack = !isBlack;
        coinCount++;

        for (int i = 0; i < coinsToFlip.Count(); i++ )
        {
            Debug.Log(coinsToFlip[i]);
            GameObject cell2 = GameObject.Find(coinsToFlip[i]);
            ChangeColor(cell2);

        }





    }

	void ChangeColor(GameObject cell)
	{

		Block b = cell.GetComponent<Block>();
        Debug.Log(b.coinName);
        b.isBlack = !b.isBlack;
        if (b.playerIndex == 0)
        {
            b.playerIndex = 1;
        }
        else
        {
            b.playerIndex = 0;
        }
		GameObject coin3 = GameObject.Find (b.coinName);

        if (b.playerIndex == 0)
        {
			coin3.transform.renderer.material.color = Color.blue;
			
		}else{
			coin3.transform.renderer.material.color = Color.red;
		}

	}
	

	
	void Createworld() 
	{
		int i = 1;
        int index = 0;
		//yield WaitForSeconds(spawnSpeed);
		for(float x  =0.0f; x<colldWidth; x+=1.02f) {
			
			
			
			//yield WaitForSeconds(spawnSpeed);
			
			for(float z  = 0.0f ; z<colldHeight ; z+=1.02f) {
				
				if(((int)x) == 0 && ((int)z) == 0)
				{
					grid[(int)x,(int)z] = "cell0";

				}
				else
				{
					
					//yield WaitForSeconds(spawnSpeed);
					block1 = Instantiate(prefab, new Vector3(prefab.transform.position.x+x, prefab.transform.position.y, prefab.transform.position.z+z), Quaternion.identity) as GameObject  ;
					block1.name = "cell"+i;
					i++;
					grid[(int)x,(int)z] = block1.name;
					
					//If x , y ar in center ... instantiate coins
					if((((int)x) == 3 || ((int)x) == 4) && ((((int)z) == 3 || ((int)z) == 4)))
					{
						Block b= block1.GetComponent<Block>();
						b.isOccupied = true;

                        b.playerIndex = index;
						GameObject coin2 = coin;
						
						coin2 = Instantiate(coin2, new Vector3(prefab.transform.position.x+x,prefab.transform.position.y + 1, prefab.transform.position.z + z), prefab.transform.rotation) as GameObject ;

                        if (index == 0)
                        {
							coin2.transform.renderer.material.color = Color.blue;
							
						}else{
							coin2.transform.renderer.material.color = Color.red;
						}
						
						coin2.name="coin"+coinCount;
						b.coinName=coin2.name;

                        if (index == 0)
                            index++;
                        else
                            index--;
                      
						coinCount++;
					}
				}
		
			}
            if (index == 0)
                index++;
            else
                index--;
		}
		
	}
}


