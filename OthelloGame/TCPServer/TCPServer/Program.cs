﻿using System;
//using UnityEngine;
using System.Runtime;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.IO;
using Othello_Binary_Buffer;
using System.Runtime.Serialization.Formatters.Binary;
//using Google.ProtocolBuffers.Examples.Othello;

namespace TCPServer{

    static class Program 
    {
        const string PolicyFileRequest = "<policy-file-request/>";
        static byte[] request = Encoding.UTF8.GetBytes(PolicyFileRequest);

        //Stupid Unity security policy
        const string AllPolicy =
        @"<?xml version='1.0' encoding=""UTF-8""?>
        <cross-domain-policy>
            <allow-access-from domain=""*"" to-ports=""*"" />
        </cross-domain-policy>";


       
        //Making builders of protofiles
        //static JoinGameRequest.Builder joinReqBuilder = JoinGameRequest.CreateBuilder();
        //static JoinGameResponse.Builder joinResBuilder = JoinGameResponse.CreateBuilder();
        //static InGameRequest.Builder inGameReqBuilder = InGameRequest.CreateBuilder();
        //static InGameResponse.Builder inGameRespBuilder = InGameResponse.CreateBuilder();
        
        ////Distionary of Socket to Client 
        static IDictionary<TcpClient, Client_tcp> clientManager = new Dictionary<TcpClient, Client_tcp>();

        //Dictionary of Rooms (room to client players)
        static Dictionary<int, List<TcpClient>> roomManager = new Dictionary<int, List<TcpClient>>();
        //Dictionary of Rooms to Grid objects
        static Dictionary<int, Grid> roomGrid = new Dictionary<int, Grid>();
       
        //Server Socket
        static  TcpListener serverSocket = new TcpListener(843);

       

        //Send to Redis
        public static string HttpPost(string URI, string Parameters)
        {
            System.Net.WebRequest req = System.Net.WebRequest.Create(URI);
            req.Proxy = new System.Net.WebProxy();
            //Add these, as we're doing a POST
            req.ContentType = "application/x-www-form-urlencoded";
            req.Method = "POST";
            //We need to count how many bytes we're sending. 
            //Post'ed Faked Forms should be name=value&
            byte[] bytes = System.Text.Encoding.ASCII.GetBytes(Parameters);
            req.ContentLength = bytes.Length;
            System.IO.Stream os = req.GetRequestStream();
            os.Write(bytes, 0, bytes.Length); //Push it out there
            os.Close();
            System.Net.WebResponse resp = req.GetResponse();
            if (resp == null) return null;
            System.IO.StreamReader sr =
                  new System.IO.StreamReader(resp.GetResponseStream());
            return sr.ReadToEnd().Trim();
        }


        //Sends acknowledgment to client
        static void sendToClient(TcpClient client, int playerIndex)
        {
           // BinaryFormatter formatter = new BinaryFormatter();
            Othello_Buffer buffer = new Othello_Buffer();
            JSONHandler jonHandle = new JSONHandler();
            buffer.requestType = "JOINRESP" ;
            buffer.name = clientManager[client].clientName;
            buffer.playerIndex = playerIndex;
            buffer.flippedCells = new List<string>();
        
             buffer.playerOneCount = 2;
             buffer.playerTwoCount = 2;
            //joinResBuilder.SetRequestType("JOINRESP");
            //joinResBuilder.SetName(clientManager[client].clientName);
            //joinResBuilder.SetPlayerIndex(playerIndex);
            //JoinGameResponse jresp = joinResBuilder.BuildPartial();
            //inGameReqBuilder = null;
            byte[] len;
            byte[] payload;
            long length;
            //using (MemoryStream stream = new MemoryStream())
            //{
            //    //Save the person to a stream
            //    formatter.Serialize(stream, buffer); 


            //    payload = stream.ToArray();
            //}
            string serialized = JSONHandler.SerializeTCPMessage(buffer);
            payload = System.Text.Encoding.ASCII.GetBytes(serialized);
            length = payload.Length;
            len = BitConverter.GetBytes(length);


            byte[] message = new byte[len.Count() + payload.Count() + 1];
            byte delimeter = 28;

            len.CopyTo(message, 0);
            payload.CopyTo(message, len.Length);
            message[message.Length - 1] = delimeter;
            NetworkStream networkStream = client.GetStream();
            networkStream.Write(message, 0, message.Length);
            networkStream.Flush();
   
        }


        //Adds client to a room (which contains two players)
        static void AddToRoom(TcpClient client)
        {
           
            int i ;
            int playerIndex = 0;
            bool roomFound = false;
        //Check for room with 1 client
            for (i = 0; i < roomManager.Count; i++)
            {
                List<TcpClient> temp = roomManager[i + 1];

                if (temp.Count == 1)
                { 
                //Room with single client is found
                    roomFound = true;
                }
            }

            if (roomFound)
            {
                //Add this client to that room
               
                roomManager[i].Add(client);
                Console.WriteLine("Player two added .." + clientManager[client].clientName+" to Room "+i);
                playerIndex++;
            }
            else
            { 
                //No rooms with single client .... create new room
                List<TcpClient> temp = new List<TcpClient>();
                Console.WriteLine("New Room created.... "+(i+1));
                temp.Add(client);
                Console.WriteLine("Player one added ...." + clientManager[client].clientName);
                roomManager[i + 1] = temp;

                //Create a new Grid
                Grid othelloBoard = new Grid();
                othelloBoard.Createworld();
                roomGrid[i + 1] = othelloBoard;
            }

            clientManager[client].clientAdded = true;
         
            //Send Join Acknowledgment to client 
            sendToClient(client, playerIndex);

            //when both players have joined a room 
            if (playerIndex == 1)
            { 
            SendToRoom( client,  "0", "GAMESTART");
            }
           
        }

        //Sends message to other of same room
        static void SendToRoom(TcpClient client, string cellNo, string status )
        {
           // BinaryFormatter formatter = new BinaryFormatter();
            Othello_Buffer buff = new Othello_Buffer();
            JSONHandler jonHandle = new JSONHandler();
            int i;
            int k = 0;
            bool roomFound = false;
            List<TcpClient> roomMember = new List<TcpClient>();
            //Find room 
            for (i = 0; i < roomManager.Count; i++)
            {
                List<TcpClient> temp = roomManager[i + 1];


                if (temp.Count == 2)
                {
                    for (k = 0; k < temp.Count; k++)
                    {
                        if (temp[k].GetHashCode() == client.GetHashCode())
                        {
                            //Clients room found
                            roomFound = true;
                            roomMember = temp;
                            break;
                        }


                    }
                }
            }
            //Fetch grid object
            Grid othelloBoard = roomGrid[i];


       
                buff.requestType = "INGAMERESP";
                buff.status = status;
                buff.playerIndex = k;
                buff.flippedCells = new List<string>();

                buff.playerOneCount = othelloBoard.playerOneCells;

                buff.playerTwoCount = othelloBoard.playerTwoCells;
                if (status == "VALID")
                {
                    for (int j = 0; j < othelloBoard.coinsToFlip.Count(); j++)
                    {
                        // inGameRespBuilder.SetFlippedCells(j, othelloBoard.coinsToFlip[j]);
                        buff.flippedCells.Add(othelloBoard.coinsToFlip[j]);
                    }

                    othelloBoard.coinsToFlip.Clear();
                    // inGameRespBuilder.SetInstantiateCell(cellNo);
                    buff.instantiateCell = cellNo;

                }
                else if (status == "GAMEENDS")
                {
                    //inGameRespBuilder.SetWinner(othelloBoard.winner);
                    buff.winner = othelloBoard.winner;
                    for (int j = 0; j < othelloBoard.coinsToFlip.Count(); j++)
                    {
                        // inGameRespBuilder.SetFlippedCells(j, othelloBoard.coinsToFlip[j]);
                        buff.flippedCells.Add(othelloBoard.coinsToFlip[j]);
                    }

                    othelloBoard.coinsToFlip.Clear();
                    // inGameRespBuilder.SetInstantiateCell(cellNo);
                    buff.instantiateCell = cellNo;


                }
                else
                {
                  

                }
            
           
            //InGameResponse iresp = inGameRespBuilder.BuildPartial();
            //inGameReqBuilder = null;
            byte[] len;
            byte[] payload;
            long length;
            //using (MemoryStream stream = new MemoryStream())
            //{
            //    //Save the person to a stream
            //   // iresp.WriteTo(stream);
            //    formatter.Serialize(stream, buff);


            //    payload = stream.ToArray();
            //}
            string serialized = JSONHandler.SerializeTCPMessage(buff);
            payload = System.Text.Encoding.ASCII.GetBytes(serialized);
            length = payload.Length;
            len = BitConverter.GetBytes(length);


            byte[] message = new byte[len.Count() + payload.Count() + 1];
            byte delimeter = 28;

            len.CopyTo(message, 0);
            payload.CopyTo(message, len.Length);
            message[message.Length - 1] = delimeter;
           
            //Room found Now send message to all in that room
            for (int j = 0; j < roomMember.Count; j++)
            {
                NetworkStream networkStream = roomMember[j].GetStream();
                networkStream.Write(message, 0, message.Length);
                networkStream.Flush();
            
            }
        
        
        }

        static byte[] trim(byte[] bytes)
        {
           // int a = 0;
            byte delimeter = 28;
            int index = Array.LastIndexOf(bytes, delimeter);
    
            Array.Resize(ref bytes, index);
            return bytes;
        }

       
        static void gameLogic(TcpClient client, string cellName)
        { 
        //Find room
            int i;
            int k =0;
            bool roomFound = false;
            List<TcpClient> roomMember = new List<TcpClient>();
            //Find room 
            for (i = 0; i < roomManager.Count; i++)
            {
                List<TcpClient> temp = roomManager[i + 1];


                if (temp.Count == 2)
                {
                    for (k =0; k < temp.Count; k++)
                    {
                        if (temp[k].GetHashCode() == client.GetHashCode())
                        {
                            //Clients room found
                            roomFound = true;
                            roomMember = temp;
                            break;
                        }


                    }
                }
            }

            if (roomFound)
            {


                Grid g = roomGrid[i];


                //This is done to check if client is black or not
                g.currentIndex = k;
               


                //Check if its a valid move
                if (g.CheckIfValidMove(cellName, "CHECK"))
                {
                    //Ok so move is valid ... Now check for effect
                    string status = "DEFAULT";
                    if (g.winner != "TBA")
                    {
                        //Game ends
                        status = "GAMEENDS";
                       // string response = HttpPost("http://127.0.0.1:5657/", "type=submit&name=" + clientManager[roomMember[Convert.ToInt32(g.winner)]].clientName + "&");
                    }
                    else
                    {
                        g.CheckForEffect(cellName);
                        if (g.winner != "TBA")
                        {
                            status = "GAMEENDS";
                        }
                        else 
                        {
                            status = "VALID";
                        }
                        
                    }
                    SendToRoom(client, cellName, status);

                }
                else
                {
                    //Check if any valid moves are left
                    if (g.CheckForValidMovesLeft())
                    {
                        SendToRoom(client, cellName, "INVALID");
                    }
                    else {
                    //No free moves left so turn change
                        SendToRoom(client, cellName, "CHANGETURN");
                    }
                
                }
        
            
            }
            
        
        }


        static bool processPacket(TcpClient client)
        {
            //BinaryFormatter formatter = new BinaryFormatter();
            Othello_Buffer buff = new Othello_Buffer();
          
            //Get Complete Buffer data from clientManager Map 
            Client_tcp clientInfo = clientManager[client];
            
            byte[] temp = clientInfo.packetData;
            byte[] bufferToProcess = new byte[clientInfo.payload_length];
            Array.Copy(temp, 8, bufferToProcess, 0, clientInfo.payload_length);
            string deserialized = System.Text.Encoding.ASCII.GetString(bufferToProcess);
            buff = JSONHandler.DeSerializeJSONString(deserialized);
            //using (Stream stream = new MemoryStream(bufferToProcess))
            //{
            //    //Save the person to a stream
            //   buff = (Othello_Buffer) formatter.Deserialize(stream);


              
            //}



            //JoinGameRequest joinGame = JoinGameRequest.CreateBuilder().MergeFrom(bufferToProcess).Build();
           // InGameRequest inGame = InGameRequest.CreateBuilder().MergeFrom(bufferToProcess).Build();



            //Check type of request
            if (buff.requestType == "JOINREQ")
            {
               // This is join request
                clientInfo.clientName = buff.name;
                    AddToRoom(client);
            
            
            
            }
            else if (buff.requestType == "INREQ")
            {
               // This is in Game request

                gameLogic(client, buff.cellno);

            
            
            }
            //if (joinGame != null && joinGame.RequestType == "JOINREQ")
            //{ 
            //    //This is join request
            //    clientInfo.clientName = joinGame.Name;
            //    AddToRoom(client);
            
            //}
            //if (inGame != null && inGame.RequestType == "INREQ")
            //{
            //    //This is in Game request

            //    gameLogic(client, inGame.Cellno);

            //}


            
            return true;
        }


        static bool Deserialize(byte[] bytes, TcpClient client)
        {
            NetworkStream networkStream = client.GetStream();
            Client_tcp clientInfo = new Client_tcp() ;
            //Check if client exists in dictionary
            if (clientManager.ContainsKey(client))
            {
                clientInfo = clientManager[client];

            }
            if (bytes != clientInfo.cacheData)
            {
                clientInfo.packetData = trim(bytes);
            }
            else
            {
                clientInfo.packetData = clientInfo.cacheData;
            }
            bool completePacket = true;

            clientInfo.resetVariables();
           


            do
            {
               

                //Checks if full header is received
                if (clientInfo.packetData.Count() + clientInfo.header_left < 8)
                {
            // Header hasnt been received 
                    clientInfo.header_left = 8 - clientInfo.packetData.Count();
                    completePacket = false;

                    //Read rest of header 

                    networkStream.Read(clientInfo.packetData, clientInfo.packetData.Count(), clientInfo.header_left);


                }
                else
                {
                    byte[] length = clientInfo.packetData;
                    Array.Resize(ref length, 8);

                    int len = BitConverter.ToInt32(length, 0);
                    clientInfo.payload_length = len;

                    //Checks if full payload is recieved
                    if (clientInfo.packetData.Count() + clientInfo.payload_left < len + 8)
                    {
            //Partial payload received
                        clientInfo.payload_left = len - (clientInfo.packetData.Count() - 8);
                        completePacket = false;
                        int currentSize = clientInfo.packetData.Count();

                        if (len > clientInfo.packetData.Count())
                        {
                            Array.Resize(ref clientInfo.packetData, len);
                        }
                        //Read rest of payload 
                        networkStream.Read(clientInfo.packetData, currentSize, clientInfo.payload_left);

                    }
                    else if (clientInfo.packetData.Count() > len + 8)  
                    {
           //partial second packet received .... 
                        Array.Copy(clientInfo.packetData, clientInfo.payload_length, clientInfo.cacheData, 0, clientInfo.packetData.Count() - clientInfo.payload_length);
                        clientInfo.cacheData = trim(clientInfo.cacheData);

                        
                           
                                completePacket = true;
                            
                        

                    }
                    else 
                    {
                        //clientInfo.packetData.Count() + clientInfo.payload_left == len + 8
 
                        completePacket = true;
                    }

                }
            } while (!completePacket);




            clientManager[client] = clientInfo;
            processPacket(client);


            //Process any extra packets... Since this will be recursive pattern ... it will handle multiple packets in one buffer
            //Deserialize(clientInfo.cacheData, client);  //As its a turn based game ... only single packet of a player is processed at a time .. else integrity of game is lost

            
            return true;
        }



        static void HandleAsyncConnection(IAsyncResult res)
        {
            
            StartAccept(); //listen for new connections again
            TcpClient client = serverSocket.EndAcceptTcpClient(res);
            Console.WriteLine(client.GetHashCode()+" is connected");
            //proceed
           
            NetworkStream networkStream = client.GetStream();
            byte[] bytesFrom = new byte[client.ReceiveBufferSize];
            networkStream.Read(bytesFrom, 0, (int)client.ReceiveBufferSize);
         
            string requestFrmClient = System.Text.Encoding.ASCII.GetString(bytesFrom);
          
            //...............................................Checking for Unity first request ..............................


         
            // compare incoming data with expected request
            for (int i = 0; i < request.Length; i++)
            {
                if (requestFrmClient[i] != request[i])
                {
                    //Check if it is a join request 

                    if (Deserialize(bytesFrom, client))
                    {
                        break;
                    
                    }

                 

                    // invalid request, close socket
                    if (!(clientManager.ContainsKey(client) && (clientManager[client].clientAdded)))
                    {
                        client.Close();
                        return;
                    }
                }
            }

            if (!clientManager.ContainsKey(client))
            {
                string temp = requestFrmClient;
                temp = temp.Substring(0, temp.IndexOf("\0"));
                if (temp.Length == request.Length)
                {
                    Console.WriteLine("got policy request, sending response");
                    // request complete, send policy
                    Byte[] sendBytes = Encoding.ASCII.GetBytes(AllPolicy);
                    networkStream.Write(sendBytes, 0, sendBytes.Length);
                    networkStream.Flush();


                }
            }




            while (clientManager.ContainsKey(client) && (clientManager[client].clientAdded))
                {
                    //  Console.Write((int)clientSocket);
                    byte[] bytesRcv = new byte[10025];
                    networkStream.Read(bytesRcv, 0, (int)client.ReceiveBufferSize);
                    string dataFromClient = System.Text.Encoding.ASCII.GetString(bytesRcv);
                    Deserialize(bytesRcv, client);
                    ////dataFromClient = dataFromClient.Substring(0, dataFromClient.IndexOf("\0"));
                    // Console.WriteLine(dataFromClient);
                    //    //dataFromClient = dataFromClient.Substring(0, dataFromClient.IndexOf("$"));
                    //    Console.WriteLine(" >> Data from client - " + dataFromClient);

                    //    SendToRoom(client, dataFromClient);
                    
                }
            
        

        }


        static void StartAccept()
        {
            serverSocket.BeginAcceptTcpClient(HandleAsyncConnection, serverSocket);
        }


        static void Main(string[] args)
        {

           
           // TcpClient clientSocket = default(TcpClient);
            serverSocket.Start();
            Console.WriteLine("Listening...");
            StartAccept();
            Console.WriteLine(" >> Server Started");
        
            Console.WriteLine(" >> exit");
            Console.ReadLine();
        }
    }
}
