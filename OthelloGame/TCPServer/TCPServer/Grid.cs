﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using UnityEngine;


namespace TCPServer
{
    public class Grid //: MonoBehaviour
    {
       // public GameObject prefab;
        public Coin coin = new Coin();
        public int coinCount = 0;
        //public char map[,]= new char[8,8];
        public Block[,] grid = new Block[8, 8];
       
        public List<string> coinsToFlip = new List<string>();
      //  GameObject block1;
        float colldWidth = 8.0f;
        float colldHeight = 8.0f;
        float spawnSpeed = 1;
        public  int playerOneCells = 2;
        public int playerTwoCells = 2;
     //   public bool isBlack = true;
        public int currentIndex = 0;
        public string winner = "TBA";


        public bool CheckForValidMovesLeft()
        {
            //For all unoccupied cells .. check if any valid move exists 
            bool isFound = false;
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if (!grid[i, j].isOccupied)
                    {
                        if (CheckIfValidMove(grid[i, j].blockName, "RECHECK"))
                         {
                             isFound = true;
                             break;
                         }
                      
                    }
                }

                if (isFound)
                {
                    break;
                }
            }


           




            return isFound;
        }


        public bool CheckIfValidMove(string objName, string type)
        {
            int col = 0;
            int row = 0;
            bool isFound = false;
            bool isValid = false;
       
            int occupiedCells = 0;
            //playerOneCells = 0;
            //playerTwoCells = 0;
            Console.WriteLine("Checking " + objName);
            //GameObject cell;
            #region
            /*
		 * Checks if objName that is clicked is part of grid 
		 */
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if (grid[i, j].blockName == objName)
                    {
                        col = j;
                        row = i;
                        isFound = true;
                        break;
                    }
                }

                if (isFound)
                {
                    break;
                }
            }
            #endregion

          //  cell = GameObject.Find(grid[row, col]);
          //  Block blk = cell.GetComponent<Block>();
            Block blk = grid[row, col];

            if (blk.isOccupied)
                isFound = false;



            if (isFound)
            {
                /*
                 * Check for adjacent cells for coin of opposite color
                 */


                if (row >= 0 && col < 8)
                {
                    //..................Bottom....................................
                    if (row <= 6 && !isValid)
                    {
                        //adjacentNode = nodeGrid[i + 1][j];  
                        //cell = GameObject.Find(grid[row + 1, col]);
                        //Block b = cell.GetComponent<Block>();
                        Block b = grid[row + 1, col];

                        if (b.isOccupied && (b.playerIndex != currentIndex))
                        {
                            //There is an adjacent cell with coin of opp color
                            isValid = true;

                        }

                    }

                    //................Bottom Left .....................................
                    if ((row <= 6) && (col > 0) && !isValid)
                    {
                        //adjacentNode = nodeGrid[i + 1][j];  
                        //cell = GameObject.Find(grid[row + 1, col]);
                        //Block b = cell.GetComponent<Block>();
                        Block b = grid[row + 1, col - 1];

                        if (b.isOccupied && (b.playerIndex != currentIndex))
                        {
                            //There is an adjacent cell with coin of opp color
                            isValid = true;

                        }
                    }

                    //..................Bottom Right................................................
                    if ((row <= 6) && (col <= 6) && !isValid)
                    {
                        //adjacentNode = nodeGrid[i + 1][j];  
                        //cell = GameObject.Find(grid[row + 1, col]);
                        //Block b = cell.GetComponent<Block>();
                        Block b = grid[row + 1, col + 1];

                        if (b.isOccupied && (b.playerIndex != currentIndex))
                        {
                            //There is an adjacent cell with coin of opp color
                            isValid = true;

                        }
                    }


                    //........................Left .....................................
                    if ((col > 0) && !isValid)
                    {
                        //adjacentNode = nodeGrid[i + 1][j];  
                        //cell = GameObject.Find(grid[row + 1, col]);
                        //Block b = cell.GetComponent<Block>();
                        Block b = grid[row , col - 1];

                        if (b.isOccupied && (b.playerIndex != currentIndex))
                        {
                            //There is an adjacent cell with coin of opp color
                            isValid = true;

                        }

                    }


                    //.......................Right.............................................
                    if ((col < 7) && !isValid)
                    {
                        //adjacentNode = nodeGrid[i + 1][j];  
                        //cell = GameObject.Find(grid[row + 1, col]);
                        //Block b = cell.GetComponent<Block>();
                        Block b = grid[row , col + 1];

                        if (b.isOccupied && (b.playerIndex != currentIndex))
                        {
                            //There is an adjacent cell with coin of opp color
                            isValid = true;

                        }
                    }


                    //................Top..................................................
                    if ((row > 0) && !isValid)
                    {
                        //adjacentNode = nodeGrid[i + 1][j];  
                        //cell = GameObject.Find(grid[row + 1, col]);
                        //Block b = cell.GetComponent<Block>();
                        Block b = grid[row - 1, col];

                        if (b.isOccupied && (b.playerIndex != currentIndex))
                        {
                            //There is an adjacent cell with coin of opp color
                            isValid = true;

                        }
                    }


                    //.....................Top Right................................
                    if ((row > 0) && (col < 7) && !isValid)
                    {
                        //adjacentNode = nodeGrid[i + 1][j];  
                        //cell = GameObject.Find(grid[row + 1, col]);
                        //Block b = cell.GetComponent<Block>();
                        Block b = grid[row - 1, col + 1];

                        if (b.isOccupied && (b.playerIndex != currentIndex))
                        {
                            //There is an adjacent cell with coin of opp color
                            isValid = true;

                        }
                    }

                    //.................Top Left..........................................
                    if ((row > 0) && (col > 0) && !isValid)
                    {
                        //adjacentNode = nodeGrid[i + 1][j];  
                        //cell = GameObject.Find(grid[row + 1, col]);
                        //Block b = cell.GetComponent<Block>();
                        Block b = grid[row - 1, col - 1];

                        if (b.isOccupied && (b.playerIndex != currentIndex))
                        {
                            //There is an adjacent cell with coin of opp color
                            isValid = true;

                        }

                    }
                }


            }
            else
            {
                return false;
            }


            //Mimic coin instantiation here 
            if (isValid && (type == "CHECK"))
            {
                blk.isOccupied = true;
                blk.playerIndex = this.currentIndex;
                Console.WriteLine("block occupied "+blk.blockName);
            }
      

            return isValid;
        }

        public void CheckForEffect(string objName)
        {
           
            int col = 0;
            int row = 0;
            bool isFound = false;
            #region
            /*
		 * Checks if objName that is clicked is part of grid 
		 */
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if (grid[i, j].blockName == objName)
                    {
                        col = j;
                        row = i;
                        Console.WriteLine("cell found " + objName + " at row " + row + " and col " + col);

                        isFound = true;
                        break;
                    }
                }
                    if (isFound)
                    {
                        break;
                    }
                
            }
            #endregion

            Console.WriteLine("One count " + playerOneCells);
            Console.WriteLine("Two count " + playerTwoCells);

            //GameObject cell = GameObject.Find(grid[row, col]);
           // Block b = cell.GetComponent<Block>();
            Block b = grid[row, col];
            if (isFound)
            {

                //Bottom
                CheckForFlip(row + 1, col, "BOTTOM", b.playerIndex);
                //Bottom Right
                CheckForFlip(row + 1, col + 1, "BOTTOMRIGHT", b.playerIndex);
                //Bottom Left
                CheckForFlip(row + 1, col - 1, "BOTTOMLEFT", b.playerIndex);
                //Top
                CheckForFlip(row - 1, col, "TOP", b.playerIndex);
                //Top Right
                CheckForFlip(row - 1, col + 1, "TOPRIGHT", b.playerIndex);
                //Top Left
                CheckForFlip(row - 1, col - 1, "TOPLEFT", b.playerIndex);
                //Left
                CheckForFlip(row, col - 1, "LEFT", b.playerIndex);
                //Right
                CheckForFlip(row, col + 1, "RIGHT", b.playerIndex);


            }

            Console.WriteLine("One count "+playerOneCells);
            Console.WriteLine("Two count " + playerTwoCells);


            //Check winning condition
            //Check for winning condition 
            playerOneCells = 0;
            playerTwoCells = 0;
            int occupiedCells = 0;
            int emptyCells = 0;
            for (int x = 0; x < 8; x++)
            {
                for (int y = 0; y < 8; y++)
                {
                    // cell = GameObject.Find(grid[a, b]);
                    //  Block c = cell.GetComponent<Block>();
                    Block c = grid[x, y];

                    if (c.isOccupied)
                    {
                        occupiedCells++;

                        if (c.playerIndex == 0)
                        {
                            playerOneCells++;
                        }
                        else
                        {
                            playerTwoCells++;
                        }
                    }
                    else
                    {
                        emptyCells++;
                    }
                }

            }


            Console.WriteLine("One count " + playerOneCells);
            Console.WriteLine("Two count " + playerTwoCells);
            Console.WriteLine("Occupied cells " + occupiedCells);
            Console.WriteLine("Empty Cells " + emptyCells);




            if (occupiedCells > 63)
            {
                if (playerOneCells > playerTwoCells)
                {
                    winner = "0";
                    Console.WriteLine("Blue wins");
                }
                else
                {
                    winner = "1";
                    Console.WriteLine("Red wins");
                }
            }


        }


        bool CheckForFlip(int row, int col, string direction, int p_currentTurn)
        {
            bool isValid = false;
            //GameObject cell;
            Console.WriteLine("At row =" + row + " col=" + col + " direction =" + direction);


            if ((row < 8) && (row >= 0) && (col < 8) && (col >= 0))
            {
                isValid = true;
            }

            if (isValid)
            {
                //cell = GameObject.Find(grid[row, col]);
                //Block b = cell.GetComponent<Block>();
                Block b = grid[row, col];
                if (b.isOccupied && b.playerIndex == p_currentTurn)
                {
                    //We have found same coin 
                   // Console.WriteLine("Coin found " + b.blockName + " at row " + row + " and col " + col);
                    return true;
                }
                else if (b.isOccupied)
                {
                    //Console.WriteLine("Coin not found");
                    //Recursively call same function 
                    if (direction == "BOTTOM")
                    {
                        if (row <= 6)
                        {
                            if (CheckForFlip(row + 1, col, direction, p_currentTurn))
                            {
                                //Logic to flip coin

                                //Find the cell and change its value
                                b = grid[row, col];
                                b.playerIndex = p_currentTurn;
                                coinsToFlip.Add(b.blockName);

                               


                                return true;
                            }
                            else
                            {
                                //No coin of same type found
                                return false;
                            }

                        }
                        else
                        {
                            //Out of bounds
                            return false;

                        }


                    }
                    else if (direction == "BOTTOMLEFT")
                    {
                        if ((row <= 6) && (col > 0))
                        {

                            if (CheckForFlip(row + 1, col - 1, direction, p_currentTurn))
                            {
                                //Logic to flip coin

                                //Find the cell and change its value
                                b = grid[row, col];
                                b.playerIndex = p_currentTurn;
                                coinsToFlip.Add(b.blockName);

                              

                                return true;
                            }
                            else
                            {
                                //No coin of same type found
                                return false;
                            }
                        }
                        else
                        {
                            //Out of bounds
                            return false;
                        }
                    }
                    else if (direction == "BOTTOMRIGHT")
                    {
                        if ((row <= 6) && (col <= 6))
                        {

                            if (CheckForFlip(row + 1, col + 1, direction, p_currentTurn))
                            {
                                //Logic to flip coin

                                //Find the cell and change its value
                                b = grid[row, col];
                                b.playerIndex = p_currentTurn;
                                coinsToFlip.Add(b.blockName);

                              


                                return true;
                            }
                            else
                            {
                                //No coin of same type found
                                return false;
                            }
                        }
                        else
                        {
                            //Out of bounds
                            return false;
                        }
                    }
                    else if (direction == "TOP")
                    {
                        if ((row > 0))
                        {

                            if (CheckForFlip(row - 1, col, direction, p_currentTurn))
                            {
                                //Logic to flip coin

                                //Find the cell and change its value
                                b = grid[row, col];
                                b.playerIndex = p_currentTurn;
                                coinsToFlip.Add(b.blockName);

                              


                                return true;
                            }
                            else
                            {
                                //No coin of same type found
                                return false;
                            }
                        }
                        else
                        {
                            //Out of bounds
                            return false;
                        }
                    }
                    else if (direction == "TOPLEFT")
                    {
                        if ((row > 0) && (col > 0))
                        {

                            if (CheckForFlip(row - 1, col - 1, direction, p_currentTurn))
                            {
                                //Logic to flip coin

                                //Find the cell and change its value
                                b = grid[row, col];
                                b.playerIndex = p_currentTurn;
                                coinsToFlip.Add(b.blockName);

                               

                                return true;
                            }
                            else
                            {
                                //No coin of same type found
                                return false;
                            }
                        }
                        else
                        {
                            //Out of bounds
                            return false;
                        }
                    }
                    else if (direction == "TOPRIGHT")
                    {
                        if ((row > 0) && (col <= 6))
                        {

                            if (CheckForFlip(row - 1, col + 1, direction, p_currentTurn))
                            {
                                //Logic to flip coin

                                //Find the cell and change its value
                                b = grid[row, col];
                                b.playerIndex = p_currentTurn;
                                coinsToFlip.Add(b.blockName);

                             

                                return true;
                            }
                            else
                            {
                                //No coin of same type found
                                return false;
                            }
                        }
                        else
                        {
                            //Out of bounds
                            return false;
                        }
                    }
                    else if (direction == "LEFT")
                    {
                        if (col > 0)
                        {

                            if (CheckForFlip(row, col - 1, direction, p_currentTurn))
                            {
                                //Logic to flip coin

                                //Find the cell and change its value
                                b = grid[row, col];
                                b.playerIndex = p_currentTurn;
                                coinsToFlip.Add(b.blockName);

                              


                                return true;
                            }
                            else
                            {
                                //No coin of same type found
                                return false;
                            }
                        }
                        else
                        {
                            //Out of bounds
                            return false;
                        }
                    }
                    else if (direction == "RIGHT")
                    {

                        if (col <= 6)
                        {

                            if (CheckForFlip(row, col + 1, direction, p_currentTurn))
                            {
                                //Logic to flip coin

                                //Find the cell and change its value
                                b = grid[row, col];
                               
                                b.playerIndex = p_currentTurn;
                                coinsToFlip.Add(b.blockName);

                               


                                return true;
                            }
                            else
                            {
                                //No coin of same type found
                                return false;
                            }
                        }
                        else
                        {
                            //Out of bounds
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }


                }
                else
                {
                    //Reached empty cell return false
                    return false;
                }

            }
            else
            {
                return false;
            }


        }

       public void Createworld()
        {
            int i = 1;

            //yield WaitForSeconds(spawnSpeed);
            for (float x = 0.0f; x < colldWidth; x += 1.02f)
            {



                //yield WaitForSeconds(spawnSpeed);

                for (float z = 0.0f; z < colldHeight; z += 1.02f)
                {

                    if (((int)x) == 0 && ((int)z) == 0)
                    {
                        Block b = new Block();
                        b.blockName = "cell0";
                        grid[(int)x, (int)z] =b;

                    }
                    else
                    {
                        Block b = new Block();
                        //yield WaitForSeconds(spawnSpeed);
                      //  block1 = Instantiate(prefab, new Vector3(prefab.transform.position.x + x, prefab.transform.position.y, prefab.transform.position.z + z), Quaternion.identity) as GameObject;
                        b.blockName = "cell" + i;
                        i++;
                       

                        //If x , y ar in center ... instantiate coins
                        if ((((int)x) == 3 || ((int)x) == 4) && ((((int)z) == 3 || ((int)z) == 4)))
                        {
                            Console.WriteLine("Cell occupied "+b.blockName);
                            b.isOccupied = true;
                           
                            b.playerIndex = currentIndex;
                            Coin coin2 = coin;

                           
                            coin2.name = "coin" + coinCount;
                            b.coinName = coin2.name;

                            if (currentIndex == 0)
                                currentIndex++;
                            else
                                currentIndex--;

                            coinCount++;
                        }
                        grid[(int)x, (int)z] = b;
                    }

                }
                if (currentIndex == 0)
                    currentIndex++;
                else
                    currentIndex--;
            }

        }
    }



}
