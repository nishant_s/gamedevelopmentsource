// Generated by ProtoGen, Version=2.4.1.521, Culture=neutral, PublicKeyToken=17b3b1f090c3ea48.  DO NOT EDIT!
#pragma warning disable 1591, 0612, 3021
#region Designer generated code

using pb = global::Google.ProtocolBuffers;
using pbc = global::Google.ProtocolBuffers.Collections;
using pbd = global::Google.ProtocolBuffers.Descriptors;
using scg = global::System.Collections.Generic;
namespace Google.ProtocolBuffers.Examples.Othello {
  
  namespace Proto {
    
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    public static partial class JoinGameRequest {
    
      #region Extension registration
      public static void RegisterAllExtensions(pb::ExtensionRegistry registry) {
      }
      #endregion
      #region Static variables
      internal static pbd::MessageDescriptor internal__static_Othello_JoinGameRequest__Descriptor;
      internal static pb::FieldAccess.FieldAccessorTable<global::Google.ProtocolBuffers.Examples.Othello.JoinGameRequest, global::Google.ProtocolBuffers.Examples.Othello.JoinGameRequest.Builder> internal__static_Othello_JoinGameRequest__FieldAccessorTable;
      #endregion
      #region Descriptor
      public static pbd::FileDescriptor Descriptor {
        get { return descriptor; }
      }
      private static pbd::FileDescriptor descriptor;
      
      static JoinGameRequest() {
        byte[] descriptorData = global::System.Convert.FromBase64String(
            "CiNvdGhlbGxvX3Byb3RvL0pvaW5HYW1lUmVxdWVzdC5wcm90bxIHT3RoZWxs" + 
            "bxokZ29vZ2xlL3Byb3RvYnVmL2NzaGFycF9vcHRpb25zLnByb3RvIjQKD0pv" + 
            "aW5HYW1lUmVxdWVzdBITCgtyZXF1ZXN0VHlwZRgBIAIoCRIMCgRuYW1lGAIg" + 
            "AigJQi5IAcI+KQonR29vZ2xlLlByb3RvY29sQnVmZmVycy5FeGFtcGxlcy5P" + 
            "dGhlbGxv");
        pbd::FileDescriptor.InternalDescriptorAssigner assigner = delegate(pbd::FileDescriptor root) {
          descriptor = root;
          internal__static_Othello_JoinGameRequest__Descriptor = Descriptor.MessageTypes[0];
          internal__static_Othello_JoinGameRequest__FieldAccessorTable = 
              new pb::FieldAccess.FieldAccessorTable<global::Google.ProtocolBuffers.Examples.Othello.JoinGameRequest, global::Google.ProtocolBuffers.Examples.Othello.JoinGameRequest.Builder>(internal__static_Othello_JoinGameRequest__Descriptor,
                  new string[] { "RequestType", "Name", });
          pb::ExtensionRegistry registry = pb::ExtensionRegistry.CreateInstance();
          RegisterAllExtensions(registry);
          global::Google.ProtocolBuffers.DescriptorProtos.CSharpOptions.RegisterAllExtensions(registry);
          return registry;
        };
        pbd::FileDescriptor.InternalBuildGeneratedFileFrom(descriptorData,
            new pbd::FileDescriptor[] {
            global::Google.ProtocolBuffers.DescriptorProtos.CSharpOptions.Descriptor, 
            }, assigner);
      }
      #endregion
      
    }
  }
  #region Messages
  [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
  public sealed partial class JoinGameRequest : pb::GeneratedMessage<JoinGameRequest, JoinGameRequest.Builder> {
    private JoinGameRequest() { }
    private static readonly JoinGameRequest defaultInstance = new JoinGameRequest().MakeReadOnly();
    private static readonly string[] _joinGameRequestFieldNames = new string[] { "name", "requestType" };
    private static readonly uint[] _joinGameRequestFieldTags = new uint[] { 18, 10 };
    public static JoinGameRequest DefaultInstance {
      get { return defaultInstance; }
    }
    
    public override JoinGameRequest DefaultInstanceForType {
      get { return DefaultInstance; }
    }
    
    protected override JoinGameRequest ThisMessage {
      get { return this; }
    }
    
    public static pbd::MessageDescriptor Descriptor {
      get { return global::Google.ProtocolBuffers.Examples.Othello.Proto.JoinGameRequest.internal__static_Othello_JoinGameRequest__Descriptor; }
    }
    
    protected override pb::FieldAccess.FieldAccessorTable<JoinGameRequest, JoinGameRequest.Builder> InternalFieldAccessors {
      get { return global::Google.ProtocolBuffers.Examples.Othello.Proto.JoinGameRequest.internal__static_Othello_JoinGameRequest__FieldAccessorTable; }
    }
    
    public const int RequestTypeFieldNumber = 1;
    private bool hasRequestType;
    private string requestType_ = "";
    public bool HasRequestType {
      get { return hasRequestType; }
    }
    public string RequestType {
      get { return requestType_; }
    }
    
    public const int NameFieldNumber = 2;
    private bool hasName;
    private string name_ = "";
    public bool HasName {
      get { return hasName; }
    }
    public string Name {
      get { return name_; }
    }
    
    public override bool IsInitialized {
      get {
        if (!hasRequestType) return false;
        if (!hasName) return false;
        return true;
      }
    }
    
    public override void WriteTo(pb::ICodedOutputStream output) {
      int size = SerializedSize;
      string[] field_names = _joinGameRequestFieldNames;
      if (hasRequestType) {
        output.WriteString(1, field_names[1], RequestType);
      }
      if (hasName) {
        output.WriteString(2, field_names[0], Name);
      }
      UnknownFields.WriteTo(output);
    }
    
    private int memoizedSerializedSize = -1;
    public override int SerializedSize {
      get {
        int size = memoizedSerializedSize;
        if (size != -1) return size;
        
        size = 0;
        if (hasRequestType) {
          size += pb::CodedOutputStream.ComputeStringSize(1, RequestType);
        }
        if (hasName) {
          size += pb::CodedOutputStream.ComputeStringSize(2, Name);
        }
        size += UnknownFields.SerializedSize;
        memoizedSerializedSize = size;
        return size;
      }
    }
    
    public static JoinGameRequest ParseFrom(pb::ByteString data) {
      return ((Builder) CreateBuilder().MergeFrom(data)).BuildParsed();
    }
    public static JoinGameRequest ParseFrom(pb::ByteString data, pb::ExtensionRegistry extensionRegistry) {
      return ((Builder) CreateBuilder().MergeFrom(data, extensionRegistry)).BuildParsed();
    }
    public static JoinGameRequest ParseFrom(byte[] data) {
      return ((Builder) CreateBuilder().MergeFrom(data)).BuildParsed();
    }
    public static JoinGameRequest ParseFrom(byte[] data, pb::ExtensionRegistry extensionRegistry) {
      return ((Builder) CreateBuilder().MergeFrom(data, extensionRegistry)).BuildParsed();
    }
    public static JoinGameRequest ParseFrom(global::System.IO.Stream input) {
      return ((Builder) CreateBuilder().MergeFrom(input)).BuildParsed();
    }
    public static JoinGameRequest ParseFrom(global::System.IO.Stream input, pb::ExtensionRegistry extensionRegistry) {
      return ((Builder) CreateBuilder().MergeFrom(input, extensionRegistry)).BuildParsed();
    }
    public static JoinGameRequest ParseDelimitedFrom(global::System.IO.Stream input) {
      return CreateBuilder().MergeDelimitedFrom(input).BuildParsed();
    }
    public static JoinGameRequest ParseDelimitedFrom(global::System.IO.Stream input, pb::ExtensionRegistry extensionRegistry) {
      return CreateBuilder().MergeDelimitedFrom(input, extensionRegistry).BuildParsed();
    }
    public static JoinGameRequest ParseFrom(pb::ICodedInputStream input) {
      return ((Builder) CreateBuilder().MergeFrom(input)).BuildParsed();
    }
    public static JoinGameRequest ParseFrom(pb::ICodedInputStream input, pb::ExtensionRegistry extensionRegistry) {
      return ((Builder) CreateBuilder().MergeFrom(input, extensionRegistry)).BuildParsed();
    }
    private JoinGameRequest MakeReadOnly() {
      return this;
    }
    
    public static Builder CreateBuilder() { return new Builder(); }
    public override Builder ToBuilder() { return CreateBuilder(this); }
    public override Builder CreateBuilderForType() { return new Builder(); }
    public static Builder CreateBuilder(JoinGameRequest prototype) {
      return new Builder(prototype);
    }
    
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    public sealed partial class Builder : pb::GeneratedBuilder<JoinGameRequest, Builder> {
      protected override Builder ThisBuilder {
        get { return this; }
      }
      public Builder() {
        result = DefaultInstance;
        resultIsReadOnly = true;
      }
      internal Builder(JoinGameRequest cloneFrom) {
        result = cloneFrom;
        resultIsReadOnly = true;
      }
      
      private bool resultIsReadOnly;
      private JoinGameRequest result;
      
      private JoinGameRequest PrepareBuilder() {
        if (resultIsReadOnly) {
          JoinGameRequest original = result;
          result = new JoinGameRequest();
          resultIsReadOnly = false;
          MergeFrom(original);
        }
        return result;
      }
      
      public override bool IsInitialized {
        get { return result.IsInitialized; }
      }
      
      protected override JoinGameRequest MessageBeingBuilt {
        get { return PrepareBuilder(); }
      }
      
      public override Builder Clear() {
        result = DefaultInstance;
        resultIsReadOnly = true;
        return this;
      }
      
      public override Builder Clone() {
        if (resultIsReadOnly) {
          return new Builder(result);
        } else {
          return new Builder().MergeFrom(result);
        }
      }
      
      public override pbd::MessageDescriptor DescriptorForType {
        get { return global::Google.ProtocolBuffers.Examples.Othello.JoinGameRequest.Descriptor; }
      }
      
      public override JoinGameRequest DefaultInstanceForType {
        get { return global::Google.ProtocolBuffers.Examples.Othello.JoinGameRequest.DefaultInstance; }
      }
      
      public override JoinGameRequest BuildPartial() {
        if (resultIsReadOnly) {
          return result;
        }
        resultIsReadOnly = true;
        return result.MakeReadOnly();
      }
      
      public override Builder MergeFrom(pb::IMessage other) {
        if (other is JoinGameRequest) {
          return MergeFrom((JoinGameRequest) other);
        } else {
          base.MergeFrom(other);
          return this;
        }
      }
      
      public override Builder MergeFrom(JoinGameRequest other) {
        if (other == global::Google.ProtocolBuffers.Examples.Othello.JoinGameRequest.DefaultInstance) return this;
        PrepareBuilder();
        if (other.HasRequestType) {
          RequestType = other.RequestType;
        }
        if (other.HasName) {
          Name = other.Name;
        }
        this.MergeUnknownFields(other.UnknownFields);
        return this;
      }
      
      public override Builder MergeFrom(pb::ICodedInputStream input) {
        return MergeFrom(input, pb::ExtensionRegistry.Empty);
      }
      
      public override Builder MergeFrom(pb::ICodedInputStream input, pb::ExtensionRegistry extensionRegistry) {
        PrepareBuilder();
        pb::UnknownFieldSet.Builder unknownFields = null;
        uint tag;
        string field_name;
        while (input.ReadTag(out tag, out field_name)) {
          if(tag == 0 && field_name != null) {
            int field_ordinal = global::System.Array.BinarySearch(_joinGameRequestFieldNames, field_name, global::System.StringComparer.Ordinal);
            if(field_ordinal >= 0)
              tag = _joinGameRequestFieldTags[field_ordinal];
            else {
              if (unknownFields == null) {
                unknownFields = pb::UnknownFieldSet.CreateBuilder(this.UnknownFields);
              }
              ParseUnknownField(input, unknownFields, extensionRegistry, tag, field_name);
              continue;
            }
          }
          switch (tag) {
            case 0: {
              throw pb::InvalidProtocolBufferException.InvalidTag();
            }
            default: {
              if (pb::WireFormat.IsEndGroupTag(tag)) {
                if (unknownFields != null) {
                  this.UnknownFields = unknownFields.Build();
                }
                return this;
              }
              if (unknownFields == null) {
                unknownFields = pb::UnknownFieldSet.CreateBuilder(this.UnknownFields);
              }
              ParseUnknownField(input, unknownFields, extensionRegistry, tag, field_name);
              break;
            }
            case 10: {
              result.hasRequestType = input.ReadString(ref result.requestType_);
              break;
            }
            case 18: {
              result.hasName = input.ReadString(ref result.name_);
              break;
            }
          }
        }
        
        if (unknownFields != null) {
          this.UnknownFields = unknownFields.Build();
        }
        return this;
      }
      
      
      public bool HasRequestType {
        get { return result.hasRequestType; }
      }
      public string RequestType {
        get { return result.RequestType; }
        set { SetRequestType(value); }
      }
      public Builder SetRequestType(string value) {
        pb::ThrowHelper.ThrowIfNull(value, "value");
        PrepareBuilder();
        result.hasRequestType = true;
        result.requestType_ = value;
        return this;
      }
      public Builder ClearRequestType() {
        PrepareBuilder();
        result.hasRequestType = false;
        result.requestType_ = "";
        return this;
      }
      
      public bool HasName {
        get { return result.hasName; }
      }
      public string Name {
        get { return result.Name; }
        set { SetName(value); }
      }
      public Builder SetName(string value) {
        pb::ThrowHelper.ThrowIfNull(value, "value");
        PrepareBuilder();
        result.hasName = true;
        result.name_ = value;
        return this;
      }
      public Builder ClearName() {
        PrepareBuilder();
        result.hasName = false;
        result.name_ = "";
        return this;
      }
    }
    static JoinGameRequest() {
      object.ReferenceEquals(global::Google.ProtocolBuffers.Examples.Othello.Proto.JoinGameRequest.Descriptor, null);
    }
  }
  
  #endregion
  
}

#endregion Designer generated code
