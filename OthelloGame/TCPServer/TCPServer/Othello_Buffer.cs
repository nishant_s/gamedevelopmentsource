﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace Othello_Binary_Buffer
{
    [Serializable] 
    public class Othello_Buffer
    {

       
        public string requestType { get; set; }
        public string name { get; set; }
        public int playerIndex { get; set; }
        public string cellno { get; set; }
        public string status { get; set; }
        public int playerOneCount { get; set; }
        public int playerTwoCount { get; set; }
        public string instantiateCell { get; set; }
        public List<string> flippedCells { get; set; }
        public string winner { get; set; }
    
    }
}
